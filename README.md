# Ubuntu OverSSH Reinstallation

![Ubuntu 20.04 bionic](https://badgen.net/badge/ubuntu/20.04/green "Ubuntu 20.04 bionic")
![Ubuntu 18.04 bionic](https://badgen.net/badge/ubuntu/18.04/green "Ubuntu 18.04 bionic")
![Ubuntu 16.04 bionic](https://badgen.net/badge/ubuntu/16.04/green "Ubuntu 16.04 bionic")

![MIT License](https://badgen.net/badge/license/MIT/orange "MIT License")

If your ISP/DataCenter doesn't provide standard or trusted iso for your server. Get ugly server from it with installed version of Ubuntu server.
Or your data is so important and you [Full System Encryption](https://help.ubuntu.com/community/ManualFullSystemEncryption) method for encrypt all data stored on your vps or server. If ISP just clone your vps harddrive? If just change raid HDD drive on dedicate server. How do you deal with it? Simple just partition encrypted drives and enjoy your secure server.

I have same situation installed ubuntu has bad partition table or huge list of stupid package installed. DataCenter network is my primary concern or it's really cheap price but administrators doesn't care. What should i do? That's my way to solve this issue easily.

Ask them to install ubuntu server what ever is it. re install it over ssh, repartition and get clean installation of ubuntu server.

Create your own net ISO and reinstall it over ssh. You can partition yor server as you desire. And set more configuration using [ubuntu preseed](https://help.ubuntu.com/lts/installation-guide/amd64/apbs02.html) just over ssh; no KVM/IPMI/VNC required.

It's simple, create modified iso based on your network and preseed url file and continue installation over ssh.

I use [Minimal CD](https://help.ubuntu.com/community/Installation/MinimalCD) because it's small and get most packages from ubuntu repository so you server after installation is fully updated.

## Preseed Server

Using docker to run preseed server via `preseed-server/docker-compose.yml`.

## Usage

### 1. Wizard

#### 1.1 Download this repo and extract it.

```bash
wget https://gitlab.com/aasaam/ubuntu-overssh-reinstallation/-/archive/master/ubuntu-overssh-reinstallation-master.tar.gz
tar -xf ubuntu-overssh-reinstallation-master.tar.gz
cd ubuntu-overssh-reinstallation-master/
```

#### 1.2 Check network discover

Use this for generate exported variable for your network.

```bash
sudo apt install net-tools
./network-discover.sh
```

#### 1.3 Preseed Server PHP script (optional)

Upload generated preseed file or using docker to run preseed server.

```bash
export PRESEED_SERVER=http://192.168.100.100
```

If your server need proxy to download latest mini iso file use this variable to download via proxy.

```bash
export ISO_DOWNLOAD_PROXY=http://192.168.200.200
```

#### 1.4 Run wizard

```bash
./wizard.sh
```

This script will be help you to create config and preseed file. `config` and `preseed.cfg` will be generate on `tmp` directory.

### 2. Make ISO

This script will be create your modified iso file based on network and preseed file.

```bash
./setup-iso.sh
```

### 3. Update GRUB

Your iso is ready and you can update grub for load the iso for first priority.

```bash
./setup-update-grub.sh
```

### 4. Reboot and wait for it

Reboot the server.

```bash
reboot
```

Wait to server reset, may take a few minites depend on your server manufacturer.

### 5. Continue installation

You can ssh login for installation, example: `installer@192.168.1.1`
