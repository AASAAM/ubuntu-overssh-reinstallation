#!/bin/bash

DEFAULT_INTERFACE=$(route | grep '^default' | grep -o '[^ ]*$')
ENOREGEX="en"
IFACES=( $(ip addr list | awk -F': ' '/^[0-9]/ {print $2}') )
C_IFACES=()

INTERFACE_DEV_NEW_NAME=

for i in "${IFACES[@]}"
do
  if [[ "$i" != "lo" ]]; then
    if [[ "$i" =~ $ENOREGEX ]]; then
      PRDNAME=$i
    else
      INTERFACE_DEV_NEW=$(udevadm test /sys/class/net/$i 2>/dev/null | grep ID_NET_NAME_PATH | cut -d "=" -f 2)
      INTERFACE_DEV_NEW_NAME=$INTERFACE_DEV_NEW
      PRDNAME=$i
    fi
  fi
  C_IFACES+=("$PRDNAME")
done

echo  -e "\033[0;31mAttention: check network carefully!\033[0m"
for i in "${C_IFACES[@]}"
do
  if [ ! -z "$i" ]; then
    INTERFACE_DEV=$i
    INTERFACE_IP=$(ifconfig $INTERFACE_DEV | sed -En -e 's/.*inet ([0-9.]+).*/\1/p')
    if [ -z "$INTERFACE_IP" ]; then
      INTERFACE_IP=$(ifconfig $INTERFACE_DEV | sed -En -e 's/.*inet addr:([0-9.]+).*/\1/p')
    fi
    INTERFACE_NETMASK=$(ifconfig $INTERFACE_DEV | awk '/netmask/{print $4}')
    if [ -z "$INTERFACE_NETMASK" ]; then
      INTERFACE_NETMASK=$(ifconfig $INTERFACE_DEV | awk '/Mask/{print $4}' | sed -En -e 's/Mask:([0-9.]+).*/\1/p')
    fi
    INTERFACE_GATEWAY=$(route -n | grep $INTERFACE_DEV | grep '^0.0.0.0' | awk '{print $2}')
    if [ ! -z "$INTERFACE_IP" ] && [ ! -z "$INTERFACE_NETMASK" ] && [ ! -z "$INTERFACE_GATEWAY" ]; then
      echo  -e "\033[0;34m$INTERFACE_DEV\033[0m:"
      if [ -z "$INTERFACE_DEV_NEW_NAME" ]; then
        echo "export INTERFACE_DEV=$INTERFACE_DEV"
      else
        echo "export INTERFACE_DEV=$INTERFACE_DEV_NEW_NAME"
      fi
      echo "export INTERFACE_IP=$INTERFACE_IP"
      echo "export INTERFACE_NETMASK=$INTERFACE_NETMASK"
      echo "export INTERFACE_GATEWAY=$INTERFACE_GATEWAY"
      echo "export INTERFACE_NAMESERVER=8.8.8.8"
    fi
  fi
done
